
/**graph1 */
var data = [{
    x: ['giraffes', 'orangutans', 'monkeys'],
    y: [20, 14, 23],
    type: 'bar'
  }];
  
  Plotly.newPlot('chartContainer1', data, {}, {showSendToCloud:true});


/**graph 2 - line chart */
var trace1 = {
    x: [1, 2, 3, 4],
    y: [10, 15, 13, 17],
    type: 'scatter',
  };
  
  var trace2 = {
    x: [1, 2, 3, 4],
    y: [16, 5, 11, 9],
    type: 'scatter'
  };
  
  var data = [trace1, trace2];
  
  Plotly.newPlot('chartContainer2', data, {}, {showSendToCloud: true});

/**graph 3 pie chart */

var data = [{
    values: [19, 26, 55],
    labels: ['Residential', 'Non-Residential', 'Utility'],
    type: 'pie'
  }];
  
  var layout = {
    height: 400,
    width: 500
  };
  
  Plotly.newPlot('chartContainer3', data, layout);

  /**graph 4 scatter */

  var trace1 = {
    x: [1, 2, 3, 4],
    y: [0, 2, 3, 5],
    fill: 'tozeroy',
    type: 'scatter'
  };
  
  var trace2 = {
    x: [1, 2, 3, 4],
    y: [3, 5, 1, 7],
    fill: 'tonexty',
    type: 'scatter'
  };
  
  var data = [trace1, trace2];
  
  Plotly.newPlot('chartContainer4', data);




  /**function to call data */
